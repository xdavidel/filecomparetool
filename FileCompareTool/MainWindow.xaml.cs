﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace FileCompareTool
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		private static StringCollection log = new StringCollection();

		public string FirstPath
		{
			get { return firstPathTextBox.Text; }
			set { firstPathTextBox.Text = value; }
		}

		public string SecondPath
		{
			get { return secondPathTextBox.Text; }
			set { secondPathTextBox.Text = value; }
		}

		public MainWindow()
		{
			InitializeComponent();
			
			FirstPath = @"C:\Program Files";
			SecondPath = @"C:\Program Files (x86)";

		}

		private void compareButton_Click(object sender, RoutedEventArgs e)
		{
			changesListView.ItemsSource = null;

			if (!Directory.Exists(FirstPath) || !Directory.Exists(SecondPath))
			{
				return;
			}

			string p1 = FirstPath;
			string p2 = SecondPath;

			progressBar.IsIndeterminate = true;
			Task<IEnumerable<string>> tsk = Task.Run(() => CompareDirs(p1, p2, true));

			tsk.ContinueWith(s =>
			{
				changesListView.ItemsSource = s.Result;
				progressBar.IsIndeterminate = false;
			},
				new CancellationToken(),
				TaskContinuationOptions.AttachedToParent, 
				TaskScheduler.FromCurrentSynchronizationContext());
			
		}

		private static IEnumerable<string> CompareDirs(string path1, string path2,bool subDirs = true)
		{
			DirectoryInfo dir1 = new DirectoryInfo(path1);
			DirectoryInfo dir2 = new DirectoryInfo(path2);

			IEnumerable<FileInfo> list1 = dir1.GetFiles("*");
			IEnumerable<FileInfo> list2 = dir2.GetFiles("*");

			List<string> retVaList = new List<string>();
			foreach (var fileInfo1 in list1)
			{
				foreach (var fileInfo2 in list2)
				{
					if (fileInfo1.Name != fileInfo2.Name) continue;
					
					if (!CompareFiles(fileInfo1, fileInfo2))
					{
						retVaList.Add($"{fileInfo1.FullName} != {fileInfo2.FullName}");
					}
				}
			}

			var comparer = new FileComparer();

			var query = (from file in list1
				select file).Except(list2,comparer);
			foreach (FileInfo info in query)
			{
				retVaList.Add($"{info.FullName} <=");
			}

			query = (from file in list2
						 select file).Except(list1, comparer);
			foreach (FileInfo info in query)
			{
				retVaList.Add($"{info.FullName} =>");
			}


			if (subDirs)
			{
				IEnumerable<DirectoryInfo> dirs1 = dir1.GetDirectories();
				IEnumerable<DirectoryInfo> dirs2 = dir2.GetDirectories();

				var commonSubDirs = from d1 in dirs1
									from d2 in dirs2
									where d1.Name == d2.Name
									select new {d1, d2};

				foreach (var dirs in commonSubDirs)
				{
					retVaList.AddRange(CompareDirs(dirs.d1.FullName,dirs.d2.FullName));
				}

			}


			return retVaList;
		}

		private static bool CompareFiles(FileInfo file1, FileInfo file2)
		{
			if (file1.Length != file2.Length)
			{
				return false;
			}

			byte[] f1 = File.ReadAllBytes(file1.FullName);
			byte[] f2 = File.ReadAllBytes(file2.FullName);


			return !f1.Where((t, i) => t != f2[i]).Any();
		}



		private void ChooseFirstDir_OnClick(object sender, EventArgs e)
		{
			Browse(firstPathTextBox);
		}

		private void ChooseSecondDir_OnClick(object sender, EventArgs e)
		{
			Browse(secondPathTextBox);
		}

		private void Browse(TextBox textBox)
		{
			var dialog = new System.Windows.Forms.FolderBrowserDialog();
			var result = dialog.ShowDialog();
			if (result == System.Windows.Forms.DialogResult.OK)
			{
				textBox.Text = dialog.SelectedPath;
			}
		}


		class FileComparer:IEqualityComparer<FileInfo>
		{
			public bool Equals(FileInfo x, FileInfo y)
			{
				return x.Name == y.Name && x.Length == y.Length;
			}

			public int GetHashCode(FileInfo obj)
			{
				return $"{obj.Name}{obj.Length}".GetHashCode();
			}
		}
	}
}
